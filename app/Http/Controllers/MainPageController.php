<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\User;

class MainPageController extends Controller
{
    /*   THIS IS THE INDEX PAGE OF THE APPLICATION */
    
    public function index(){
        
        $data = [
            'user' => count(User::all()),
            'employee' => count(Employee::all())
        ];
        
        return view('welcome', $data);
    }
}
