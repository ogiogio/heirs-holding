<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userType = RoleUser::where("user_id", auth()->user()->id)->value('role_id'); 
        
        if($userType == 2){
            $idata =  Employee::latest()->paginate(30);
        }else{
           $idata =   Employee::where("id", auth()->user()->id)->latest()->paginate(30);
        }
     
        
        $data = ['all_emp' => $idata, 'usertype' => $userType];
        
        return view('home', $data)
                ->with('i', (request()->input('page', 1) - 1) * 50);
    }
    
    
    
    public function allUsers() {
        
        $userType = RoleUser::where("user_id", auth()->user()->id)->value('role_id'); 
        
        if($userType == 2){
            $idata =  User::latest()->paginate(30);
        }else{
           $idata =   User::where("id", auth()->user()->id)->latest()->paginate(3);
        }
        
        $data =  ['all_emp' => $idata, 'usertype' => $userType];
        
        return view('users', $data)
                ->with('i', (request()->input('page', 1) - 1) * 50);
    }
    
    
   
    
    public function addEmployee(){
        
        return view('add_employee');
    }
    
    
    
    
    
    public function create(){
        
    }
    
    
    
    
    public function update(){
        
    }
}
