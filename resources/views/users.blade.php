@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('List of Users') }}</div>

                <div class="card-body">
                    
                     @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p></p>
                        </div>
                    @endif
    
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                          <a href="/" class="btn btn-secondary">Home <span class="fa fa-user"></span></a>
                        @if($usertype == 2)
                        <a href="{{  route('home') }}" class="btn btn-success">All Employee <span class="fa fa-user">+</span></a>
                         <a href="{{  route('addE') }}" class="btn btn-danger">Add Employee <span class="fa fa-user">+</span></a>
                         <a href="{{  route('users') }}" class="btn btn-primary">All Users <span class="fa fa-user">+</span></a>
                        @endif
                        <hr/>
                    </div>
                   
                    <div>
                       
                        @if(count($all_emp) > 0)
                        <table class="table table-responsive" id="holdings">
                            <thead>
                            <tr>
                                <th>S/n</th>
                                <th>Firstname</th>
                                 <th>email</th>
                                 <th>Date Created</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            @foreach ($all_emp as $key => $emp)
                             <tr>
                                <td> {{ $emp->id }}</td>
                                 <td>{{ $emp->name }}</td>
                                 <td>{{ $emp->email }}</td>
                                 <td>{{ $emp->created_at }}</td>
                            </tr>
                            @endforeach
                             </tbody>
                            
                        </table>
                        @else
                        <div>Your Records have not been captured!. Please contact Human Resources</div>
                        
                        @endif
                        
                    </div>
                   
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
