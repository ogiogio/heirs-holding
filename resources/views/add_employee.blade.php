@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> <br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div>
                        <a href="{{ route('home') }}" class="btn btn-danger"> Back </a>
                        <hr/>
                    </div>
                   
                    <div>
                        <h2>Add Employee</h2>
                    </div>
                    
                      <div class="card-body">
                    <form method="POST" action="{{ route('add_employee.store') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">First Name</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="" >
                                

                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Last Name</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="" required autofocus>

                            </div>
                        </div>
                        
                         <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Staff Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="" required autofocus>

                            </div>
                        </div>
                        
                        
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Date of Birth</label>

                            <div class="col-md-6">
                                <input id="dateofbirth" type="date" class="form-control" name="dateofbirth"  required autofocus>

                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">First Name</label>

                            <div class="col-md-6">
                                <select class="form-control" name="department"  id="department" >
                                     <option value="">Select</option>
                                      <option value="Information Technology">Information Technology</option>
                                     <option value="Human Resources">Human Resources</option>
                                      <option value="Account">Account</option>
                                       <option value="Corporate Communications">Corporate Communications</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="row mb-3">
                            
                            <div class="col-md-6 offset-md-5">
                                <input type="submit" value="Add Employee" class="btn btn-primary btn-sm" />
                            </div>
                        </div>
                        
                    </form>
                      </div>
                   
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
