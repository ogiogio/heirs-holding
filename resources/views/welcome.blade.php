<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Holding Human Resource </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" rel="stylesheet">
        
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <!-- Styles -->
      
    </head>
    <body>
        
       
        
      <header>
        <h1>Holding Employee Management System</h1>
      </header>
        
        <div class="main_header">
            @if (Route::has('login'))
                <div class="">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
        
        
        <!-- Dashboard Section -->
    <center>
        <table class="pullcenter">
            <tr>
                <td> 
                    <div class="m_cardings"> 
                        <a href="{{ route('home') }}"> All Employees {{ $employee }} </a>
                </div>
                </td>
                
                <td> 
                    <div class="m_cardings"> 
            <a href="{{ route('users') }}"> Total Users {{ $user }}</a>
        </div>
                </td>
            </tr>
            
             <tr>
                <td> 
                    <div class="m_cardings"> 
                   <a href=""> Salary Paid (0)</a>
                </div>
                </td>
                
                <td> 
                    <div class="m_cardings"> 
          <a href="">  Leave Request (0)</a>
        </div>
                </td>
            </tr>
        </table>
        
    </center>
       
        
       
        
        
        
        
        
        
        
        
    </body>
</html>
