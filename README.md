
## About Heirs Holding

Heirs Holding is a simple Human Resource managment system built to manage entry and exit for staff and applicant.


## Configuration Options

- [Run composer install]
- [Open the .env file and enter your database credentials]
- [if .env file does not exist copy the .env.example into .env]
- [The default crentials are ]

DB_DATABASE=emp_mgr
DB_USERNAME=root
DB_PASSWORD=

- You can change it based on your system requirement.
- If you intend to use the default credentials ensure that they are setup in your phpmyadmin or mysql as may be required.


## Running the App
- Open the run folder or cd into the root folder "heirholdings"
- Run the command php artisan migrate  
- Run the command php artisan db:seed
- Run the commain "php artisan serve"
- You application will run on http://localhost:8000
- If you encounter an error while trying to serve the page using the following command
php artisan key:generate.

Congratulations you can now launch the app from your browser
