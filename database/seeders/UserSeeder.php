<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\RoleUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idata =  User::create([
             'name' => 'holdings', 
             'email' => 'holdings@gmail.com',
             'password' => Hash::make('123456789'),
        ]);
        
        RoleUser::create([
            'role_id' => 1,
            'user_id' => $idata->id,
        ]);
        
        $data =  User::create([
             'name' => 'Ogiogio Victor', 
             'email' => 'ogiogiovictor@gmail.com',
             'password' => Hash::make('lovejoy'),
        ]);
         
        RoleUser::create([
            'role_id' => 2,
            'user_id' => $data->id,
        ]);
    }
}


